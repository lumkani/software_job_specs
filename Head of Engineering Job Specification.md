# Head of Engineering 
```
Department: Technical (Software + Hardware)

Application closing date: 2021, 20 June

Reports to: CEO / Head of Product 

Direct reports: Head of Software, Head of Hardware

Location: Cape Town / Remote 

Travel required: None

Applications to be sent to: paul@lumkani.com 

Salary: Market Related

```
# **Who we are:**

Lumkani uses technology to mitigate household risksand provide financial services to
previously under-served markets in South Africa. Weprovide short and long-term insurance to
increase resilience in low-income households and enableour clients to thrive.

We are a young company – no idea to improve what wedo is off limits. We strive to test, break,
re-test, and embed ideas that work. Our high-functioningteams are based in Johannesburg and
Cape Town.

We are groundbreaking in the field of IoT, and ourhardware has already been distributed to
more than 40 thousands homes and has won many awards.

# **Who you are:**

A technically minded people’s person. You will beinvolved in the high level management of a strong team of engineers and software developers.You have excellent attention to detail and you are organised and self-driven. You get pleasurein optimising technical processes, motivating and managing technical teams and creatinggreat relationships with stakeholders.

You are ultimately responsible for all technical employees,their management and performance as well as technical suppliers/stakeholders. You havegreat planning and management skills, and you are able to prioritise and define key goals.You have knowledge of best development
practices in software development, cloud technologiesand ideally hardware development.


# **Role Description:**

The Head of Engineering is responsible for a numberof key areas of work:

## **1. Technical Personnel:**
### Internal:
* All technical hiring + the hiring strategy (interviews,the hiring process, sourcing talent).
* Constantly improving onboarding - working alongsideHead of Software (HoS) and Head of Hardware (HoH).
* Technical performance management.
* 1 on 1 with the technical team.

### Outsourcing relationships (e.g. software outsourcing):

- Making these partnership decisions.
- Maintaining these relationships.
- Defining quality and ensuring that we achieve thatstandard.

### Consultants + Interns:

- Including the existing Embedded Systems consultants,and Business
    Intelligence consultants.

## **2. Project Management:**

You will be responsible for planning longer term hardware+ software projects:

- Building plans alongside the HoH + HoS in relationto research and
    development projects.
- Presenting the plan to the broader business to getbuy-in.
- Managing these projects, checking in regularly onprogress, updating
    expectations across the business, assisting in tryingto solve structural
    blockers. Building timelines + estimated costs.

## **3. Security, Quality and Cost:**
In close consultation with the HoS and HoH, you will ensure that technical decisions are in line with best practice, that they are within budget, and are executed with a high level of quality and maintainability.

You will be responsible for the uptime of the systems,and making sure that systems have sufficient observability and monitoring.


## **4. Technology Integrations + Stakeholder Management**
You will be responsible for:
- Managing all technical supplier relationships
- Doing DD’s on partner organisations / suppliers (e.g.analysis of the ERP systems that would be best suited for our stock managementsystem needs)
- Handling supplier changes in pricing
- Handling contractual agreements with suppliers

You will be responsible for maintaining and improving all business integrations and the technical parts of business operations. Example ofthis include:

- CRM integrations (Zendesk, Whatsapp, SMS shortcodes,call center)
- Payment Collection (Easypay, debit orders and alternatives)
- Business Intelligence (data warehousing, dashboards)

## **5. Technical Operations**
You will need to make decisions on which tools andtechnologies Lumkani should be using to enable efficiencies in the business. (e.gGoogle Suites, Telephony Solutions, Lead Management) You will make the necessary informationavailable to team leaders, ensuring that they are using these tools properly

You will also need to hire or manage people responsible for the following:

### Stock Management

- Product Stock tracking, monitoring and feedback
- Managing returned stock
- Field Agent tools (Cellphones)

### Technical Field Issues:

- Managing technical issue tracking and faults
- End to end management of in-field stock issues

## **6. Executive Functions:**

You will be the Data Protection Officer at Lumkani,ensuring that we comply with POPIA regulations.

You will be actively involved in contributing to theLumkani Technology Team Culture.

You will be responsible for handling the technologyaspects of an investment due
diligence process.

You will be involved in the technology aspects ofannual financial audits.

You will be responsible for understanding the shortcomingsand the technical debt of
Lumkani, and building strategies to address theseissues.

Lastly, you will be responsible for general businessdecisions associated with technology, which includes presenting to businessabout key technological decisions, project plans and resource requirements.

# **Requirements**

### **You must:**

- Have exceptional interpersonal skills with at least 4 years management experience 
- Be willing to work in a team and build consensus.
- Have strong technical skills, with at least 3 years experience in Business Analysis, Software Development, Hardware Development, Product/ProjectManagement or Engineering 
- Have a Bachelor degree in Management, Engineering,Software Development, Business Analysis or similar
- Be a natural problem solver

- Be excited about sharing and learning

- Be able to work autonomously

- Have strong research skills

### **You will benefit from:**

- Python and Django Experience
- Experience with AWS or equivalent Cloud Technologies
- Embedded Systems and Hardware Development experience


