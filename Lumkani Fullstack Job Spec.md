# Lumkani Fullstack Job Spec 

![Safer Together](LUMKANI-LOGO-A-2018.png)
## About us:
[Lumkani](www.lumkani.com) builds risk mitigating, internet connected hardware. We prevent the spread of fires in informal settlements - which saves lives and possessions, and creates resilience in underserved communities. As well as cutting edge hardware, we have been developing a suite of innovative financial products that aim to improve financial inclusion and break down cycles of poverty.
We are groundbreaking in the field of IoT, and our hardware has already been widely distributed and has won many awards.

## Work Culture:
Our work culture is focused on largely autonomous projects that are managed by the developers, with the support of management. We are focused on considered iterations, and agile development. We recognize that people are the at the core of the business and make sure that developers are well supported and cared for.

## Our hiring process:
We like to conduct a face to face interview first to get to know one another and make sure there is a cultural fit. 

Please research and come with questions.

We will then issue a coding test, and discuss formalities.

Applicants can send CVs and transcripts to paul@lumkani.com

## Perks:
* Stock options
* Team breakfasts
* Innovations focus
* Casual work environment
* Small friendly supportive team
* Life saving, high impact technology

## Technologies:
* Amazon EC2
* Amazon S3
* Apache Cordova
* [Quasar](https://quasar-framework.org/)
* Vue.js
* Django
* Javascript
* MySQL
* Python
* nginx
* Android Studio
* Bitbucket
* Docker
* Git
* New Relic


## The Position
We are constantly developing and refactoring our existing IoT platform, agent management tool, payment collection engine, and agent app.

As a fullstack engineer you will be required to input on all of these projects, and work closely with other team members to build cohesive, maintainable and scalable systems.

The current stack revolves around AWS, [Django](www.djangoproject.com) and [Vue.js](https://vuejs.org/). We really like the power, scalability and dynamic nature of python. We appreciate the large python [community](https://github.com/django/django) and think that it is a very suitable tool for our needs. We are excited about the elegance and rapid growth of [Vue](https://github.com/vuejs/vue) and have decided to take it on as our JS framework of choice.

The current platform manages a IoT system of fire detectors that prevent fires in informal settlements across South Africa, as well as managing insurance agents that operate in these communities - offering the first of its kind household insurance for people living in informal settlements. This position will require to deploy new features to the payment, BI and agent management backend. You will spend on CI servers, python forums, discussing architectural benefits and generally trying to focus on clean, scalable code.  

It is an exciting position that would include working with an array of technologies, a position that you can make your own, in a team that is passionate and inclusive. Your technical decisions to have a large impact on the system - you will be contributing massively to a live saving technology, and a system that hopes to improve financial inclusion in South Africa and the world.

## Requirements:

You must:

* Have at least 2 year relevant work experience / similar
* Be able to show us code you are proud of
* Be diligent, with strong attention to details such as naming conventions
* Strive to create clean code
* Naturally create reusable components
* Emphasize testability and maintainability
* Be based in Cape Town or willing to move to Cape Town
* Be a natural problem solver
* Be willing to work closely in a team
* Be excited about sharing and learning
* Be able to work autonomously
* Have strong research skills

You will benefit from:

* Experience with python and Django
* Experience with AWS (Cloudfront, S3, EC2)
* Experience with unit tests
* Experience with JS


