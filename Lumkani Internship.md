# Lumkani Internship Application 

![Safer Together](LUMKANI-LOGO-A-2018.png)
## About us:
[Lumkani](www.lumkani.com) builds risk mitigating, internet connected hardware. We prevent the spread of fires in informal settlements - which saves lives and possessions, and creates resilience in underserved communities. As well as cutting edge hardware, we have been developing a suite of innovative financial products that aim to improve financial inclusion and break down cycles of poverty.
We are groundbreaking in the field of IoT, and our hardware has already been widely distributed and has won many awards.
We are going through the process of refactoring our software platform that supports our hardware network, and are hiring a new in-house software team to support the new developments.

## Internship Culture:
We want to take on 3 month internships or longer term vacation work applicants. Our work culture is focused on largely autonomous projects. We will support you and are excited to discuss any issues, accept any improvements or feedback, and created dedicated learning sessions during out weekly Lumkani Labs sessions.

 We are focused on considered iterations, and agile development. We recognize that people are the at the core of the business and make sure that interns well supported and cared for.

 A note for applicants undergoing studies: we would like to create longer term relationships and projects that could span over multiple vac work periods.

## Our application process:
We like to conduct initial 20 min Skype interviews first to get to know one another and make sure there is a cultural fit. 

Applicants can send CVs and transcripts to paul@lumkani.com

Please research and come with questions.

We will then issue a coding test, and discuss formalities.

## Perks:
* Team breakfasts
* Innovations focus
* Casual work environment
* Small friendly supportive team
* Life saving, high impact technology

## Technologies:
* Amazon EC2
* Amazon S3
* Apache Cordova
* Quasar
* Vue.js
* C
* Django
* Javascript
* MySQL
* php
* Python
* Jquery
* nginx
* Android Studio
* Bitbucket
* Docker
* Git
* New Relic


## The Internship
We are in constant development and refactoring of our existing IoT platform, agent management tool, payment collection engine, and agent app.

As an intern you will be you will be provided with a project that suits your skills and passions, and work closely with other team members to build cohesive, maintainable and scalable solutions. You will present your work on a two and three month cycle.

The current stack revolves around AWS, [Django](www.djangoproject.com) and [Vue.js](https://vuejs.org/). We really like the power, scalability and dynamic nature of python. We appreciate the large python [community](https://github.com/django/django) and think that it is a very suitable tool for your needs. We are excited about the elegance and rapid growth of [Vue](https://github.com/vuejs/vue) and have decided to take it on as our JS framework of choice.

The current platform manages a IoT system of fire detectors that prevent fires in informal settlements across South Africa, as well as managing insurance agents that operate in these communities - offering the first of its kind household insurance for people living in informal settlements. This position will require to deploy new features to the payment, BI and agent management backend. You will spend time on CI servers, python forums, discussing architectural benefits and generally trying to focus on clean, scalable code.   

## Requirements:

You must:

* be able to show us code you are proud of
* Be diligent, with strong attention to details such as naming conventions
* Strive to create clean code
* Naturally create reusable components
* Emphasize testability and maintainability
* Be based in Cape Town or willing to move to Cape Town
* Be a natural problem solver
* Be willing to work closely in a team
* Be excited about sharing and learning
* Be able to work autonomously
* Have strong research skills

You will benefit from:

* Experience with python and Django
* Experience with AWS (Cloudfront, S3, EC2)
* Experience with unit tests
* Experience with JS


